<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class MoveFile extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:moveFile';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Move file "archivo.csv"  From /pedropablo To /api-woocommerce/storage/app';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $initialTime = microtime(true);

        $this->info('Corriendo comandos');
        $this->runCommands();
        $this->info('Archivo copiado');

        $totalTime = microtime(true);
        $time = $totalTime - $initialTime;
        $minutes = $time / 60;
        
        $this->info('Termino ' . $time . ' segundos ' . $minutes . ' minutos');
    }
    public function runCommands(){
        $this->info('Copiando archivo');
        exec('cp /home/pedropablo/archivo.csv /home/api-woocommerce-mundo/storage/app/');
    }
}
