<?php

namespace App\Console\Commands;

use App\Models\ImageFail;
use App\Models\Product;
use Automattic\WooCommerce\Client;
use Illuminate\Console\Command;
use PhpParser\Node\Stmt\Echo_;

class updateProducts extends Command
{
    protected $woocommerce;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:updateProducts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->woocommerce = new Client(
            'https://lineavital.com.co',
            'ck_000483333991aa86ba63d5f0a0a88d9a8e8bb028',
            'cs_8c73907659ff987820060ae7525e1748c0f35780',
            [
                'version' => 'wc/v3',
                'timeout' => 800,
            ]
        );
        $initialTime = microtime(true);
        ImageFail::truncate();
        $this->updateProducts();
        $finalTime = microtime(true);
        $time = $finalTime - $initialTime;
        $minutes = $time / 60;
        $this->info('termino ' . $time . ' segundos ' . $minutes . ' minutos');
    }
    public function updateProducts()
    {

       
        $productsCount = Product::count();
        $offset = 0;
        $products = $this->woocommerce->get('products', ['per_page' => 100]);
        foreach ($products as $product) {
            if (Product::where('sku', $product->sku)->exists()) {
                $productTmp = Product::where('sku', $product->sku)->get();
                if ($product->images) {
                    $this->woocommerce->put('products/' . $product->id . '', [
                        'stock_quantity' => $productTmp[0]->stock_quantity,
                        'regular_price' => $productTmp[0]->regular_price,
                        'sale_price' => $productTmp[0]->sale_price != "0" ? $productTmp[0]->sale_price : '',
                    ]);
                    $this->info($product->sku . ' Stock y precio');
                } else {
                    $urlexists = $this->url_exists($productTmp[0]->url);
                    if ($urlexists) {
                        $this->woocommerce->put('products/' . $product->id . '', [
                            'images' => [
                                [
                                    'src' => $productTmp[0]->url
                                ]
                            ],
                            'regular_price' => $productTmp[0]->regular_price,
                            'stock_quantity' => $productTmp[0]->stock_quantity,
                            'sale_price' => $productTmp[0]->sale_price != "0" ? $productTmp[0]->sale_price : '',

                        ]);
                        $this->info($product->sku . ' Imagen, stock, precio');
                    } else {
                        //si no es una imagen que exista(enlace roto)
                        ImageFail::create([
                            'sku' => $product->sku
                        ]);
                        $this->woocommerce->put('products/' . $product->id . '', [
                            'regular_price' => $productTmp[0]->regular_price,
                            'stock_quantity' => $productTmp[0]->stock_quantity,
                            'sale_price' => $productTmp[0]->sale_price != "0" ? $productTmp[0]->sale_price : '',


                        ]);
                        $this->info($product->sku . ' Stock y precio');
                    }
                }
            }
        }
        $offset = $offset + 100;
        if ($offset > 0) {
            while ($offset <= $productsCount) {
                $this->info($offset);
                $products = $this->woocommerce->get('products', ['per_page' => 100, 'offset' =>  $offset]);
                foreach ($products as $product) {
                    if (Product::where('sku', $product->sku)->exists()) {
                        $productTmp = Product::where('sku', $product->sku)->get();
                        if ($product->images) {

                            $this->woocommerce->put('products/' . $product->id . '', [
                                'stock_quantity' => $productTmp[0]->stock_quantity,
                                'regular_price' => $productTmp[0]->regular_price,
                                'sale_price' => $productTmp[0]->sale_price != "0" ? $productTmp[0]->sale_price : '',


                            ]);
                            $this->info($product->sku . ' Stock y precio');
                        } else {
                            $urlexists = $this->url_exists($productTmp[0]->url);
                            if ($urlexists) {
                                $this->woocommerce->put('products/' . $product->id . '', [
                                    'images' => [
                                        [
                                            'src' => $productTmp[0]->url
                                        ]
                                    ],
                                    'regular_price' => $productTmp[0]->regular_price,
                                    'stock_quantity' => $productTmp[0]->stock_quantity,
                                    'sale_price' => $productTmp[0]->sale_price != "0" ? $productTmp[0]->sale_price : '',


                                ]);
                                $this->info($product->sku . ' Imagen, stock y precio');
                            } else {
                                $this->woocommerce->put('products/' . $product->id . '', [
                                    'regular_price' => $productTmp[0]->regular_price,
                                    'stock_quantity' => $productTmp[0]->stock_quantity,
                                    'sale_price' => $productTmp[0]->sale_price != "0" ? $productTmp[0]->sale_price : '',


                                ]);
                                $this->info($product->sku . ' Stock y precio');
                            }
                        }
                    }
                }
                $this->info(' #### Nuevo lote actualizado #### ');

                $offset = $offset + 100;
            }
        }
    }
    public function url_exists($url = NULL)
    {

        if (empty($url)) {
            return false;
        }

        $ch = curl_init($url);

        // Establecer un tiempo de espera
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);

        // Establecer NOBODY en true para hacer una solicitud tipo HEAD
        curl_setopt($ch, CURLOPT_NOBODY, true);
        // Permitir seguir redireccionamientos
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        // Recibir la respuesta como string, no output
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // Descomentar si el servidor requiere un user-agent, referrer u otra configuración específica
        // $agent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36';
        // curl_setopt($ch, CURLOPT_USERAGENT, $agent)

        $data = curl_exec($ch);

        // Obtener el código de respuesta
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        //cerrar conexión
        curl_close($ch);

        // Aceptar solo respuesta 200 (Ok), 301 (redirección permanente) o 302 (redirección temporal)
        $accepted_response = array(200, 301, 302);
        if (in_array($httpcode, $accepted_response)) {
            return true;
        } else {
            return false;
        }
    }
}
