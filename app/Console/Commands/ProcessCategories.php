<?php

namespace App\Console\Commands;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Console\Command;
use Automattic\WooCommerce\Client;


class ProcessCategories extends Command
{
    protected $woocommerce;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:processCategories';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->woocommerce = new Client(
            'https://lineavital.com.co',
            'ck_000483333991aa86ba63d5f0a0a88d9a8e8bb028',
            'cs_8c73907659ff987820060ae7525e1748c0f35780',
            [
                'version' => 'wc/v3',
                'timeout' => 800,

            ]
        );
        $initialTime = microtime(true);
        $this->info('Consultando categorias');
        $this->getProductsCategories();
        $this->info('✔️');
        $finalTime = microtime(true);
        $time = $finalTime - $initialTime;
        $minutes = $time / 60;
        $this->info('termino ' . $time . ' segundos ' . $minutes . ' minutos');
    }
    public function getProductsCategories()
    {
        $categories = Product::all('category_1');
        $this->info('Creando categorias temporales');
        $this->createCategoryTmp($categories);
    }
    public function createCategoryTmp($categories)
    {
        foreach ($categories as  $category) {
            if ($category->name !== null) {
                Category::firstOrCreate(
                    [
                        'name' =>  $category->category_1
                    ]
                );
            }
           
        }
        $this->createCategoryWoocommerce();
    }
    public function createCategoryWoocommerce()
    {
        $categories = Category::all();
        $this->info('Creando categorias woocommerce');
        foreach ($categories as $category) {
            if ($category->id === null && $category->name !== null) {
                $categorytmp = $this->woocommerce->post('products/categories', ['name' => $category->name]);
                Category::where("name", $category->name)->update(["id" => $categorytmp->id]);
                $update = Product::where("category_1", $category->name)
                ->update(["category_id" => $category->id_local]);
                $this->info('Categoria ' . $category->name . ' creada');
            } else {
                $update = Product::where("category_1", $category->name)
                ->update(["category_id" => $category->id_local]);
                $this->info('Categoria ' . $category->name . ' ya existe');
            }
        }
    }
}
