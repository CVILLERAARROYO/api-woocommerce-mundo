<?php

namespace App\Console\Commands;

use App\Models\Product;
use Automattic\WooCommerce\Client;
use Illuminate\Console\Command;

class CreateProducts extends Command
{
    protected $woocommerce;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:createProducts';


    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->woocommerce = new Client(
            'https://lineavital.com.co',
            'ck_000483333991aa86ba63d5f0a0a88d9a8e8bb028',
            'cs_8c73907659ff987820060ae7525e1748c0f35780',
            [
                'version' => 'wc/v3',
                'timeout' => 800,

            ]
        );
        $initialTime = microtime(true);
        $this->info('Consultando productos');
        $this->getProducts();
        $this->info('✔️');
        $finalTime = microtime(true);
        $time = $finalTime - $initialTime;
        $minutes = $time / 60;
        $this->info('termino ' . $time . ' segundos ' . $minutes . ' minutos');
    }
    public function getProducts()
    {
        $productsCount = Product::all()->count();
        $skip = 0;
        if ($skip <= 0) {
            $products = Product::with('categories:id_local,id')->take(100)->get()->toArray();
            foreach ($products as $key => $product) {
                $products[$key]['categories'] = [
                    $product['categories']
                ];
            }
            $data = ['create' => $products];
            $this->woocommerce->post('products/batch', $data);
            echo 'Primer lote creado ';
            $skip = $skip + 100;
            $lote = 2;
            if ($skip > 0) {
                while ($skip <= $productsCount) {
                    $products = Product::with('categories:id_local,id')->skip($skip)->take(100)->get()->toArray();
                    foreach ($products as $key => $product) {
                        $products[$key]['categories'] = [
                            $product['categories']
                        ];
                    }
                    $data = ['create' => $products];
                    $this->woocommerce->post('products/batch', $data);
                    echo ' Lote ' . $lote . ' creado ';
                    $skip = $skip + 100;
                    $lote = $lote + 1;
                }
            }
        }
    }
}
