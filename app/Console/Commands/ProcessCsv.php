<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Traits\ImportCsvTrait;
use App\Models\Product;

class ProcessCsv extends Command 
{
    use ImportCsvTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:processCsv';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Read csv and save records on DB';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $initialTime = microtime(true);
        Product::truncate();
        $this->info('Importando productos desde archivo.csv');
        $this->import();
        $this->info('Productos importados');
        $totalTime = microtime(true);
        $time = $totalTime - $initialTime;
        $minutes = $time / 60;

        $this->info('Termino ' . $time . ' segundos ' . $minutes . ' minutos');
    }
}
