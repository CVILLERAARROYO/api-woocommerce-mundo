<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('command:moveFile')
            ->dailyAt('06:00')
            ->emailOutputTo('publicidadlineavital@gmail.com')
            ->emailOutputOnFailure('carlosmariovilleraarroyo@gmail.com');
        $schedule->command('command:processCsv')->dailyAt('06:01')
            ->emailOutputTo('publicidadlineavital@gmail.com')
            ->emailOutputOnFailure('carlosmariovilleraarroyo@gmail.com');
        $schedule->command('command:processCategories')->dailyAt('06:02')
            ->emailOutputTo('publicidadlineavital@gmail.com')
            ->emailOutputOnFailure('carlosmariovilleraarroyo@gmail.com');
        $schedule->command('command:createProducts')->dailyAt('06:05')
            ->emailOutputTo('publicidadlineavital@gmail.com')
            ->emailOutputOnFailure('carlosmariovilleraarroyo@gmail.com');
        $schedule->command('command:updateProducts')->dailyAt('06:10')
            ->emailOutputTo('publicidadlineavital@gmail.com')
            ->emailOutputOnFailure('carlosmariovilleraarroyo@gmail.com');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }

    protected function scheduleTimezone()
    {
        return 'America/Bogota';
    }
}
