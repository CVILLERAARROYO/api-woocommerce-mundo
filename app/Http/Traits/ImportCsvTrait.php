<?php

namespace App\Http\Traits;

use App\Imports\ProductsImport;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\Category;

trait ImportCsvTrait
{
    public function import()
    {
        Excel::import(new ProductsImport, 'archivo.csv');
        return Category::all();
    }
}
