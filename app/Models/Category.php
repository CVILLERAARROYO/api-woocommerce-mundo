<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $primaryKey = 'id_local';
    protected $hidden = ['id_local'];
    
    protected $fillable = ['name', 'id'];

    public function products()
    {
        return $this->hasMany(Product::class);
    }

}

