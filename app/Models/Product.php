<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $hidden = ['id','category_1','category_id'];
    protected $fillable = ['sku', 'name', 'url', 'short_description', 'type', 'regular_price', 'category_id', 'category_1', 'manage_stock', 'stock_quantity', 'sale_price', 'category_2', 'category_id_2'];
    
    public function categories()
    {
        return $this->belongsTo(Category::class , 'category_id', 'id_local');
    }
}
