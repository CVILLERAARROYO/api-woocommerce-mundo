<?php

namespace App\Imports;

use App\Models\Product;
use Maatwebsite\Excel\Concerns\{ToModel, WithCustomCsvSettings, WithChunkReading, WithBatchInserts, WithHeadingRow};
use Mockery\Undefined;

class ProductsImport implements ToModel, WithCustomCsvSettings, WithChunkReading, WithBatchInserts, WithHeadingRow
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {    
        return new Product([
            'sku'     => $row['codigo_producto'],
            'name'     => $row['descripcion'],
            'url'    => $row['url'],
            'short_description'    => $row['prescripcionmedica'],
            'regular_price'    => $row['precio'],
            'category_1'    => $row['categoria'],
            'category_2'    => isset($row['laboratorio']) ? $row['laboratorio'] : null,
            'manage_stock'    =>  true,
            'stock_quantity'    => $row['stock'],
            'sale_price'    => $row['preciooferta'] 
        ]);
       
    }
    // Read document
    public function chunkSize(): int
    {
        return 1000;
    }
    // Save record
    public function batchSize(): int
    {
        return 1000;
    }
    // Format cvs
    public function getCsvSettings(): array
    {
        return [
            'input_encoding' => 'ISO-8859-1',
            'delimiter' => ";",
        ];
    }
    public function headingRow(): int
    {
        return 1;
    }
}
