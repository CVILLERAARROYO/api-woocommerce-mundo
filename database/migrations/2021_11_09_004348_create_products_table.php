<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('sku');
            $table->string('name')->nullable();
            $table->longText('url')->nullable();
            $table->longText('short_description')->nullable();
            $table->string('type')->nullable();
            $table->string('regular_price')->nullable();
            $table->string('category_1')->nullable();
            $table->string('manage_stock')->nullable();
            $table->integer('stock_quantity')->nullable();
                        
            $table->foreignId('category_id')
            ->nullable()
            ->references('id_local')->on('categories')
            ->onDelete('SET NULL'); 
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
